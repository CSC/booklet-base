FROM python:slim
LABEL maintainer="CSC Technical Manager <csc-technical-manager@cern.ch>"
RUN mkdir -p /usr/share/man/man1 && apt-get update && apt-get install -y \
    fontconfig \
    git \
    pdftk \
    texlive-xetex \
    && rm -rf /var/lib/apt/lists/*
RUN mkdir -p /usr/share/fonts/Google
COPY fonts/Google/* /usr/share/fonts/Google/
RUN fc-cache
